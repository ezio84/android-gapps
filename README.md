**Pie GApps Mini**

**Based on Banks GApps**

**MINI:**
* Includes core Google apks
* Doesn't remove custom rom built-in stuff

**HOW TO USE:**
* Run mkgapps.sh script
* Completed Gapp zips will be in the out directory

**PREREQUISITES:**
* A full Linux system with an Android build system setup
* OpenJDK 8

**NOTES:**
* Feel free to use, change, improve, adapt these gapps. Just remember to share them!

**CREDITS:**
* Opengapps, Banks, Beans, Surge, Nathan, Ezio, Josh, Edwin, Alex, and the Android Community
